package com.example.studentdataeditor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignUpActivity extends AppCompatActivity {
    private FirebaseAuth fireBaseAuth;
    EditText etNimSecondary;
    EditText etNamaSecondary;
    Button btnEditSecondary;
    Button btnSignInSecondary;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        btnEditSecondary = findViewById(R.id.btnEditSecondary);
        btnSignInSecondary = findViewById(R.id.btnSignInSecondary);
        etNimSecondary = findViewById(R.id.etNimSecondary);
        etNamaSecondary = findViewById(R.id.etNamaSecondary);
        fireBaseAuth = FirebaseAuth.getInstance();

        btnEditSecondary.setOnClickListener(view -> {
            String email = etNimSecondary.getText().toString();
            String password = etNamaSecondary.getText().toString();

            signUp(email, password);
        });

        btnSignInSecondary.setOnClickListener(view -> {
            signIn();
        });
    }

    public void signUp(String email, String password){
        fireBaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = fireBaseAuth.getCurrentUser();
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("createUserWEmail:fail", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void signIn(){
        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
