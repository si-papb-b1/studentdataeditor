package com.example.studentdataeditor;

public class Student {
    private String id;
    private String nim;
    private String name;
    private String courseId;

    public Student(){}

    public Student(String nim, String name, String courseId){
        this.nim = nim;
        this.name = name;
        this.courseId = courseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
}
