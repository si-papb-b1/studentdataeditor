package com.example.studentdataeditor;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    RecyclerView rv;
    ArrayList<Student> data;
    TextView etNim;
    TextView etNama;
    Button bt1;
    Spinner spinner;
    String prodi;

    private static String[] spinnerList = {"Pendidikan Teknologi Informasi",
                                    "Sistem Informasi",
                                    "Teknik Komputer",
                                    "Teknologi Informasi",
                                    "Teknik Informatika"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = findViewById(R.id.rv1);

        etNim = findViewById(R.id.etNim);
        etNama = findViewById(R.id.etNama);
        bt1 = findViewById(R.id.bt1);
        spinner = findViewById(R.id.spinner);

        ArrayAdapter<String>spinAdapter = new ArrayAdapter<String>(HomeActivity.this, android.R.layout.simple_spinner_item, spinnerList);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        prodi = "1";
                        break;
                    case 1:
                        prodi = "2";
                        break;
                    case 2:
                        prodi = "3";
                        break;
                    case 3:
                        prodi = "4";
                        break;
                    case 4:
                        prodi = "5";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bt1.setOnClickListener(view -> {
            String nim = etNim.getText().toString();
            String name = etNama.getText().toString();
            saveData(nim, name, prodi);
        });

        Query query = FirebaseDatabase.getInstance().getReference().child("Student");
        FirebaseRecyclerOptions<Student> options = new FirebaseRecyclerOptions.Builder<Student>().setQuery(query, Student.class).build();


        FirebaseRecyclerAdapter<Student, StudentViewHolder> adapter = new FirebaseRecyclerAdapter<Student, StudentViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull StudentViewHolder holder, int position, @NonNull Student student) {
                DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("Course");
                Query courseQuery = ref2.orderByChild("id").equalTo(student.getCourseId());

                courseQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                        if (snapshot.exists()){
                            holder.tvProdi.setText(snapshot.child("name").getValue(String.class));
                        }
                        else{
                            holder.tvProdi.setText(student.getCourseId());
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });


                holder.tvNim.setText(student.getNim());
                holder.tvNama.setText(student.getName());

                holder.btnEdit.setOnClickListener(view -> {
                    Intent intent = new Intent(HomeActivity.this, EditingActivity.class);
                    intent.putExtra("nim", student.getNim());
                    intent.putExtra("name", student.getName());
                    intent.putExtra("prodi", holder.tvProdi.getText().toString());
                    HomeActivity.this.startActivity(intent);
                });

                holder.btnHapus.setOnClickListener(view -> {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                    Query studentQuery = ref.child("Student").orderByChild("nim").equalTo(student.getNim());

                    studentQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            for (DataSnapshot studentSnapshot: snapshot.getChildren()){
                                studentSnapshot.getRef().removeValue();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.d("onCancelled", String.valueOf(error.toException()));
                        }
                    });
                });
            }

            @NonNull
            @Override
            public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);


                return new StudentViewHolder(view);
            }
        };

    adapter.startListening();
    rv.setLayoutManager(new LinearLayoutManager(this));
    rv.setAdapter(adapter);

    }


    class StudentViewHolder extends RecyclerView.ViewHolder{
        TextView tvNim;
        TextView tvNama;
        TextView tvProdi;
        Button btnEdit;
        Button btnHapus;
        public StudentViewHolder(@NonNull View itemView){
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvProdi = itemView.findViewById(R.id.tvProdi);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnHapus = itemView.findViewById(R.id.btnHapus);
        }
    }


    public void saveData(String name, String nim, String prodi){
        if (!validateForm())
            return;
        Student student = new Student(nim, name, prodi);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference();
        DatabaseReference studentReference = dbRef.child("Student");

        String key = studentReference.push().getKey();

        studentReference.child(key).setValue(student);


    }

    public boolean validateForm(){
        boolean result = true;
        if (TextUtils.isEmpty(etNim.getText().toString())){
            etNim.setError("Required");
            result = false;
        } else {
            etNim.setError(null);
        }

        if (TextUtils.isEmpty(etNama.getText().toString())){
            etNama.setError("Required");
            result = false;
        } else {
            etNama.setError(null);
        }
        return result;
    }



}
