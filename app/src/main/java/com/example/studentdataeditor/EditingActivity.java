package com.example.studentdataeditor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class EditingActivity extends AppCompatActivity {
    Intent intent;
    String nim;
    String name;
    String prodi;

    TextView etNim;
    TextView etNama;
    TextView etProdiSecondary;
    Button btnEditSecondary;
    Spinner spinner;

    private static String[] spinnerList = {"Pendidikan Teknologi Informasi",
            "Sistem Informasi",
            "Teknik Komputer",
            "Teknologi Informasi",
            "Teknik Informatika"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halaman_pengeditan);

        etNim = findViewById(R.id.etNimSecondary);
        etNama = findViewById(R.id.etNamaSecondary);
        btnEditSecondary = findViewById(R.id.btnEditSecondary);

        spinner = findViewById(R.id.spinner2);
        ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(EditingActivity.this, android.R.layout.simple_spinner_item, spinnerList);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        prodi = "1";
                        break;
                    case 1:
                        prodi = "2";
                        break;
                    case 2:
                        prodi = "3";
                        break;
                    case 3:
                        prodi = "4";
                        break;
                    case 4:
                        prodi = "5";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        intent = getIntent();
        nim = intent.getStringExtra("nim");
        name = intent.getStringExtra("name");
        prodi = intent.getStringExtra("prodi");

        etNim.setText(nim);
        etNama.setText(name);

        btnEditSecondary.setOnClickListener(view -> {
            Intent intent2 = new Intent(this, HomeActivity.class);

            Student student = new Student();

            String nimOld = nim;

            nim = etNim.getText().toString();
            name = etNama.getText().toString();

            student.setName(name);
            student.setCourseId(prodi);
            student.setNim(nim);

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            Query studentQuery = ref.child("Student").orderByChild("nim").equalTo(nimOld);

            studentQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot snap: snapshot.getChildren()){
                        snap.getRef().setValue(student);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.d("FailedEdit", error.getMessage());
                }
            });


            startActivity(intent2);
        });

    }
}
